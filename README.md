Crear en base de datos la siguiente tabla:

-- Table: public.asignatura_persona

-- DROP TABLE public.asignatura_persona;

CREATE TABLE public.asignatura_persona
(
    persona_cedula integer,
    asignatura_codigo integer,
    CONSTRAINT asignatura_fk FOREIGN KEY (asignatura_codigo)
        REFERENCES public.asignatura (codigo) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT persona_fk FOREIGN KEY (persona_cedula)
        REFERENCES public.persona (cedula) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.asignatura_persona
    OWNER to postgres;