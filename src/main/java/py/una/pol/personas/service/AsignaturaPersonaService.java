package py.una.pol.personas.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaPersonaDAO;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.AsignaturaPersona;

import java.util.List;
import java.util.logging.Logger;

@Stateless
public class AsignaturaPersonaService {
	
    @Inject
    private Logger log;

    @Inject
    private AsignaturaPersonaDAO dao;
    
    public void crear(AsignaturaPersona ap) throws Exception {
        log.info("Enlazando asignatura-persona: " + ap.getCedula() + " " + ap.getCodigo());
        try {
        	dao.insertar(ap);
        }catch(Exception e) {
        	log.severe("ERROR al enlazar asignatura-persona: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Enlace creado con éxito");
    }
    
    public AsignaturaPersona seleccionarEnlace(AsignaturaPersona ap) {
    	return dao.seleccionarEnlace(ap);
    }
    
    public List<AsignaturaPersona> seleccionarPorCedula(long cedula) {
    	return dao.seleccionarPorCedula(cedula);
    }
    
    public List<AsignaturaPersona> seleccionarPorCodigo(long codigo) {
    	return dao.seleccionarPorCodigo(codigo);
    }
    
    public long borrar(AsignaturaPersona ap) throws Exception {
    	return dao.borrar(ap);
    }
}
