package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {
	
    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;
    
    public void crear(Asignatura a) throws Exception {
        log.info("Creando Asignatura: " + a.getNombre());
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getNombre());
    }
    
    public void actualizar(Asignatura a) throws Exception {
        log.info("Actualizando Asignatura: " + a.getCodigo());
        try {
        	dao.actualizar(a);
        }catch(Exception e) {
        	log.severe("ERROR al actualizar asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura actualizada con éxito: " + a.getNombre());
    }
    
    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignatura seleccionarPorCodigo(long codigo) {
    	return dao.seleccionarPorCodigo(codigo);
    }
    
    public long borrar(long codigo) throws Exception {
    	return dao.borrar(codigo);
    }

}
