package py.una.pol.personas.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.AsignaturaPersona;

@Stateless
public class AsignaturaPersonaDAO {
	
    @Inject
    private Logger log;
    
    public long insertar(AsignaturaPersona ap) throws SQLException {

        String SQL = "INSERT INTO asignatura_persona(persona_cedula, asignatura_codigo) "
                + "VALUES(?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
        	
        	pstmt.setLong(1, ap.getCedula());
            pstmt.setLong(2, ap.getCodigo());

 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    }
    
	public AsignaturaPersona seleccionarEnlace(AsignaturaPersona ap) {
		String SQL = "SELECT * FROM asignatura_persona WHERE " + "(persona_cedula = ?) " + "and (asignatura_codigo = ?)";
		
		AsignaturaPersona asigper = null;
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, ap.getCedula());
        	pstmt.setLong(2, ap.getCodigo());
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		asigper = new AsignaturaPersona();
        		asigper.setCedula(rs.getLong(1));
        		asigper.setCodigo(rs.getLong(2));
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return asigper;

	}
	
	public List<AsignaturaPersona> seleccionarPorCedula(long cedula) {
		String SQL = "SELECT persona_cedula, asignatura_codigo FROM asignatura_persona WHERE persona_cedula = ? ";
		
		List<AsignaturaPersona> lista = new ArrayList<AsignaturaPersona>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, cedula);
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		AsignaturaPersona ap = new AsignaturaPersona();
        		ap.setCedula(rs.getLong(1));
        		ap.setCodigo(rs.getLong(2));
        		
        		lista.add(ap);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public List<AsignaturaPersona> seleccionarPorCodigo(long codigo) {
		String SQL = "SELECT persona_cedula, asignatura_codigo FROM asignatura_persona WHERE asignatura_codigo = ? ";
		
		List<AsignaturaPersona> lista = new ArrayList<AsignaturaPersona>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, codigo);
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		AsignaturaPersona ap = new AsignaturaPersona();
        		ap.setCedula(rs.getLong(1));
        		ap.setCodigo(rs.getLong(2));
        		
        		lista.add(ap);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
    
    public long borrar(AsignaturaPersona ap) throws SQLException {

        String SQL = "DELETE FROM asignatura_persona WHERE " + "(persona_cedula = ?) " + "and (asignatura_codigo = ?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, ap.getCedula());
            pstmt.setLong(2, ap.getCodigo());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }

}
