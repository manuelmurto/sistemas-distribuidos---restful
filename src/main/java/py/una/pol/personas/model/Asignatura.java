package py.una.pol.personas.model;

import java.io.Serializable; 
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignatura implements Serializable {
	
	Long codigo;
	String nombre;
	
	
	public Asignatura(Long acodigo, String anombre){
		this.nombre = anombre;
		this.codigo = acodigo;
	}

	public Asignatura() {
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
