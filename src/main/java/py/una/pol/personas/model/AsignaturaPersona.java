package py.una.pol.personas.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@XmlRootElement
public class AsignaturaPersona implements Serializable {
	
	Long cedula;
	Long codigo;
	
	public AsignaturaPersona(Long acodigo, Long pcedula){
		this.cedula = pcedula;
		this.codigo = acodigo;
	}
	
	public AsignaturaPersona() {
	}
	
	public Long getCedula() {
		return cedula;
	}
	public void setCedula(Long cedula) {
		this.cedula = cedula;
	}
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
}
