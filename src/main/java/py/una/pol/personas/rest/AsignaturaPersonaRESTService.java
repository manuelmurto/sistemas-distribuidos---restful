package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.AsignaturaPersona;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.AsignaturaPersonaService;


@Path("/asignatura-persona")
@RequestScoped
public class AsignaturaPersonaRESTService {
	
    @Inject
    private Logger log;

    @Inject
    AsignaturaPersonaService asignaturaPersonaService;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(AsignaturaPersona ap) {

        Response.ResponseBuilder builder = null;

        try {
            asignaturaPersonaService.crear(ap);
            // Create an "ok" response
            
            //builder = Response.ok();
            builder = Response.status(201).entity("Enlace creado exitosamente");
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    @GET
    @Path("asignaturas_de_persona/{cedula:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AsignaturaPersona> obtenerPorId(@PathParam("cedula") long cedula) {
        List<AsignaturaPersona> ap = asignaturaPersonaService.seleccionarPorCedula(cedula);
        if (ap == null) {
        	log.info("obtenerPorId " + cedula + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + cedula + " encontrada ");
        return ap;
    }
    
    @GET
    @Path("personas_de_asignatura/{codigo:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AsignaturaPersona> obtenerPorCodigo(@PathParam("codigo") long codigo) {
        List<AsignaturaPersona> ap = asignaturaPersonaService.seleccionarPorCodigo(codigo);
        if (ap == null) {
        	log.info("obtenerPorCodigo " + codigo + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorCodigo " + codigo + " encontrada ");
        return ap;
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public Response borrar(AsignaturaPersona ap)
    {      
 	   Response.ResponseBuilder builder = null;
 	   try {
 		  log.severe("ASD ASD"+ ap.getCedula() + "ASD " + ap.getCodigo());
 		   if(asignaturaPersonaService.seleccionarEnlace(ap) == null) {
 			   
 			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Enlace no existe.");
 		   }else {
 			  log.severe("ASD ASD"+ ap.getCedula() + "ASD " + ap.getCodigo());
 			   asignaturaPersonaService.borrar(ap);
 			   builder = Response.status(202).entity("Enlace borrado exitosamente.");			   
 		   }
 		   
 	   } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

}
